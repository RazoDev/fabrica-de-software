const gulp  = require('gulp'),
      sass    = require('gulp-sass')
      cleanCSS = require('gulp-clean-css')
      concatJS = require('gulp-concat')
      concatCss = require("gulp-concat-css")
      htmlmin = require('gulp-htmlmin')
      uglify  = require('gulp-uglify')
      pump = require('pump')
      imagemin = require('gulp-imagemin')
      browserSync = require('browser-sync').create()
      reload = browserSync.reload;

// gulp.task('minify-css', () => {
//   return gulp.src(['app/css/*','!app/css/*.css.map'])
//     .pipe(cleanCSS({compatibility: 'ie8'}))
//     .pipe(gulp.dest('dist/css'));
// });



gulp.task('sass', function () {
    return gulp.src('app/assets/scss/index.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(concatCss("main.css"))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist/css'));
});

gulp.task('sass-intercambio', function () {
    return gulp.src('app/intercambio-libre/scss/index.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(concatCss("styles.css"))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist/css'));
});

gulp.task('minify', (cb) =>{
    pump([
        gulp.src(['app/js/*.js', '!app/js/*.min.js']),
        concatJS('main.js'),
        uglify(),
        gulp.dest('dist/js')
    ],
    cb
);
});


gulp.task('htmlminify', function() {
    return gulp.src('app/**/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('dist/'));
});

gulp.task('imgmin', () => {
gulp.src('app/img/*')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/img'))
});

gulp.task('imgIntercambio', () => {
gulp.src('app/intercambio-libre/img/*')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/intercambio-libre/img'))
});

gulp.task('fonts', () => {
  return gulp.src(['app/assets/fonts/*.*'])
        .pipe(gulp.dest('dist/fonts/'))
})

gulp.task('serve', ['sass','sass-intercambio', 'htmlminify', 'minify', 'imgmin','imgIntercambio', 'fonts'], () => {
    browserSync.init(['dist/css/*.css','app/intercambio-libre/scss/*.scss','app/intercambio-libre/img/*','dist/**/*.html','dist/js/*.js','app/img/*','fonts/*.*'], {
        port: 3030,
        ui: {
          port: 8080
        },
        server: {
            baseDir: 'dist',
        }
    });
});


gulp.task('watch', ['sass', 'sass-intercambio', 'sass-intercambio','htmlminify', 'minify', 'serve', 'imgmin', 'fonts'], function() {
    gulp.watch('app/**/*.scss', ['sass','sass-intercambio']);
    gulp.watch('app/**/*.html', ['htmlminify']);
    gulp.watch('app/js/*.js', ['minify']);
    gulp.watch('app/img/*', ['imgmin']);
    gulp.watch('app/intercambio-libre/img/*', ['imgIntercambio']);
    gulp.watch('app/assets/fonts/*.*', ['fonts']);
});

gulp.task('default', ['watch']);
